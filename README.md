# TL;DR: 🐟

A `tldr` command implemented in `fish`.

## Usage

```
Usage: tldr [OPTIONS] [COMMAND]

positional arguments are:
  COMMAND               get tldr page for COMMAND

optional arguments are:
  -h, --help            show this help message and exit
  -V, --version         show version information and exit
or:
  -c WHEN, --colour=WHEN
                        colourise output;
                        WHEN can be ‘always’, ‘auto’ or ‘never’
  -f FILE, --file=FILE  render contents of a specific tldr file
  -p PLATFORM, --platform=PLATFORM
                        set platform: ‘linux’, ‘osx’, ‘sunos’ or ‘windows’
  -u, --update          download updated tldr pages from tldr.sh
```

## Requirements

* `fish`

For updating the TLDR pages:

* `curl`, `wget` or `wget2`
* `stat` and `touch` (coreutils)
* `bsdtar` (libarchive) or `unzip`
