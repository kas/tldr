#!/usr/bin/env fish

# Meta
set -g TLDR_NAME                        'tldr'
set -g TLDR_VERSION                     '2022.11.04'
set -g TLDR_REVISION                    '1'
set -g TLDR_LICENSE                     'GNU General Public License v3+'

# Defaults
set -g TLDR_COLOUR_BLANK_DEFAULT        normal   # TCB
set -g TLDR_COLOUR_COMMAND_DEFAULT      yellow   # TCC
set -g TLDR_COLOUR_DESCRIPTION_DEFAULT  brgreen  # TCD
set -g TLDR_COLOUR_EXAMPLE_DEFAULT      white    # TCE
set -g TLDR_COLOUR_NAME_DEFAULT         brblue   # TCE
set -g TLDR_COLOUR_PARAMETER_DEFAULT    blue     # TCP

# Someday we shall have wordwrap
set -g TLDR_WIDTH_DEFAULT               74

# Constants
set -g TLDR_TRY_HELP                    "Try “$TLDR_NAME --help” for more information."
set -g TLDR_PLATFORMS                   linux osx sunos windows
set -g FISH_COLOURS                     black blue cyan green magenta \
                                              red white yellow

set -g TLDR_ZIP                         tldr.zip
set -g TLDR_ASSETS_URL                  https://tldr.sh/assets/$TLDR_ZIP
set -g TLDR_CURL_OPTIONS                -s -S -L -f -z $TLDR_ZIP -o
set -g TLDR_WGET_OPTIONS                -q -N -O
set -g TLDR_BSDTAR_OPTIONS              -xf
set -g TLDR_UNZIP_OPTIONS               -uoaq

# Write a bunch of lines
function tldr_write_lines
  # Here-documents would be sweet, but
  # this is as close as we can get
  printf '%s\n' $argv
end # tldr_write_lines

# Write a bunch of lines to stderr
function tldr_debug
  set -q TLDR_DEBUG
    and tldr_write_lines $argv >&2
end # tldr_debug

# Exit gracefully, possibly with an explanation
function tldr_die
  if test -n "$argv"
    tldr_write_lines $argv >&2
    exit 1
  end
  exit 0
end # tldr_die

# Show them a pretty little help text
function tldr_help
  set -l help_text \
    'Usage: tldr [OPTIONS] [COMMAND]' \
    '' \
    'positional arguments are:' \
    '  COMMAND               get tldr page for COMMAND' \
    '' \
    'optional arguments are:' \
    '  -h, --help            show this help message and exit' \
    '  -V, --version         show version information and exit' \
    'or:' \
    '  -c WHEN, --colour=WHEN' \
    '                        colourise output;' \
    '                        WHEN can be ‘always’, ‘auto’ or ‘never’' \
    '  -f FILE, --file=FILE  render contents of a specific tldr file' \
    '  -p PLATFORM, --platform=PLATFORM' \
    '                        set platform: ‘linux’, ‘osx’, ‘sunos’ or ‘windows’' \
    '  -u, --update          download updated tldr pages from tldr.sh'
  tldr_write_lines $help_text

  set -l epilog \
    '' \
    'Fork me on Codeberg: https://codeberg.org/kas/tldr'
  tldr_write_lines $epilog
end # tldr_help

# Which version are we running?
function tldr_version
  printf 'tldr/%s-%s\n' $TLDR_VERSION $TLDR_REVISION
end # tldr_version

function tldr_check_colour --description "Check that a colour name is know by fish" --argument-names cvar cval
  set variants '' 'br'
  contains "$cval" 'normal' $variants$FISH_COLOURS
    or tldr_die "Unrecognized colour name in $cvar=\"$cval\"" \
		"Available colours are:" \
		(string join ', ' "  "$FISH_COLOURS) \
		(string join ', ' "br"$FISH_COLOURS)
end # tldr_check_colour

function tldr_init_colours --description "Set up our colour scheme" --argument-names when
  test "$when" = 'never'
    and return

  for mumble in BLANK COMMAND DESCRIPTION EXAMPLE NAME PARAMETER
    set us  TLDR_COLOR_$mumble
    set gb  TLDR_COLOUR_$mumble
    set def (string join '' $gb _DEFAULT)
    if not set -q $gb
      set -q $us
        and set -g $gb $$us
	 or set -g $gb $$def
    end
    tldr_check_colour $gb $$gb
  end

  set -q TLDR_DEBUG
    and tldr_debug \
      "TLDR_COLOUR_BLANK=\"$TLDR_COLOUR_BLANK\"" \
      "TLDR_COLOUR_COMMAND=\"$TLDR_COLOUR_COMMAND\"" \
      "TLDR_COLOUR_DESCRIPTION=\"$TLDR_COLOUR_DESCRIPTION\"" \
      "TLDR_COLOUR_EXAMPLE=\"$TLDR_COLOUR_EXAMPLE\"" \
      "TLDR_COLOUR_NAME=\"$TLDR_COLOUR_NAME\"" \
      "TLDR_COLOUR_PARAMETER=\"$TLDR_COLOUR_PARAMETER\""

end # tldr_init_colours

# Set up environment
#
# $TLDR_HOME
#  ├── local
#  └── pages
#      ├── common
#      ├── linux
#      ├── osx
#      ├── sunos
#      └── windows
#
# ‘local’ is your own tldr pages and should shadow downloaded
# tldr pages.
# ‘common’ is common to all platforms and will be placed at the
# end of $TLDR_PATH.
# platform specific pages are inserted betwen local and common.
#
# Default:
# $TLDR_HOME/local:TLDR_HOME/pages/$platform:TLDR_HOME/pages/common
function tldr_init --description 'Set up TLDR environment'
  # $TLDR_PATH
  set -q XDG_DATA_HOME
    or set -g XDG_DATA_HOME ~/.local/share
  set -q TLDR_HOME
    or set -g TLDR_HOME "$XDG_DATA_HOME/tldr"
  if not set -q TLDR_PATH
    set -g TLDR_PATH "$TLDR_HOME/local"
    set platform (uname | string lower)
    test $platform = 'darwin'
      and set platform 'osx'
    contains $platform $TLDR_PLATFORMS
      and set -g TLDR_PATH (string join ':' $TLDR_PATH $TLDR_HOME/pages/$platform)
    set -g TLDR_PATH (string join ':' $TLDR_PATH $TLDR_HOME/pages/common)
  end

  set -q TLDR_DEBUG
    and tldr_debug \
      "XDG_DATA_HOME=\"$XDG_DATA_HOME\"" \
      "TLDR_HOME=\"$TLDR_HOME\"" \
      "TLDR_PATH=\"$TLDR_PATH\""

  # $TLDR_COLOURS
  if set -q TLDR_COLORS  # Perhaps they used the US spelling
    set -q TLDR_COLOURS
      or set -g TLDR_COLOURS $TLDR_COLOR
  end
  if set -q TLDR_COLOURS
    # Let's check the value
    if not contains "$TLDR_COLOURS" always auto never
      set oldval (string lower "$TLDR_COLOURS")
      switch "$oldval"
        case always auto never
	  set newval "$oldval"
	case yes y true t ja j 1
	  set newval 'always'
	case no nein n false f 0
	  set newval 'never'
	case '*'
	  set newval 'auto'  # FIXME: or complain?
      end
      set -g TLDR_COLOURS "$newval"
    end
  else
    # If they have $NO_COLOR set, no matter the value,
    # turn of colours, otherwise set it to ‘auto’:
    set -q NO_COLOR  # https://no-color.org/
      and set -g TLDR_COLOURS 'never'
       or set -g TLDR_COLOURS 'auto'
    # FIXME: some systems seem to test for $CLICOLOR
  end
  if test "$TLDR_COLOURS" = 'auto'
    test -t 1  # Is stdout a TTY?
      and set -g TLDR_COLOURS 'always'
       or set -g TLDR_COLOURS 'never'
  end
  # $TLDR_COLOURS is now set to either ‘always’ or ‘never’

  set -q TLDR_DEBUG
    and tldr_debug "TLDR_COLOURS=\"TLDR_COLOURS\""

  tldr_init_colours $TLDR_COLOURS
end # tldr_init

# Show a specific TLDR page
function tldr_show --description "Show a specific TLDR page" --argument-names mdfile
  test -n "$mdfile"
    or tldr_die "FILE cannot be empty"
  test -r "$mdfile"
    or tldr_die "Cannot read TLDR page ‘$mdfile’"

  if test "$TLDR_COLOURS" = "never"
    set tcb  ''
    set tcc  ''
    set tcd  ''
    set tce  ''
    set tcn  ''
    set tcp  ''
    set bold ''
  else
    set tcb  (set_color    $TLDR_COLOUR_BLANK)        #
    set tcc  (set_color    $TLDR_COLOUR_COMMAND)      # `
    set tcd  (set_color -i $TLDR_COLOUR_DESCRIPTION)  # >
    set tce  (set_color    $TLDR_COLOUR_EXAMPLE)      # -
    set tcn  (set_color -o $TLDR_COLOUR_NAME)         # #
    set tcp  (set_color    $TLDR_COLOUR_PARAMETER)    # {{
    set bold (set_color -o $TLDR_COLOUR_EXAMPLE)      # •
  end
  set bullet "$bold•$tcb"

  # Let's go, chaps
  while read line
    switch "$line"
      case '- *'
        set out (string sub --start 3 -- "$line")
	while string match --quiet --regex '^.*`.+`.*$' -- "$out"
	  set out (string replace '`' "$tcc" -- "$out")
	  set out (string replace '`' "$tcb" -- "$out")
	end
	set out "$bullet $out"
      case '`*`'
        set out (string sub --start 2 -- "$line")
	set out (string replace       '`'  "$tcb" -- "  $tcc$out")
	set out (string replace --all "{{" "$tcp" -- "$out")
	set out (string replace --all "}}" "$tcc" -- "$out")
      case '> *'
        set out (string sub --start 3 -- "$line")
	while string match --quiet --regex '^.*`.+`.*$' -- "$out"
	  set out (string replace '`' "$tcb$tcc" -- "$out")
	  set out (string replace '`' "$tcd" -- "$out")
	end
	set out "$tcd$out$tcb"
      case '# *'
        set out (string upper (string sub --start 3 -- "$line"))
	#set out (string split '' "$out")
	set out "$tcn┄ $out$tcb"
      case '*'
        set out "$line"
    end
    printf '%s\n' "$out"
  end < "$mdfile"
end # tldr_show

# Let's see if we can find a TLDR page
function tldr_find_and_show --description "Find a TLDR page in PLATFORM or TLDR_PATH" --argument-names command
  test -n "$command"
    or tldr_die "COMMAND cannot be empty"
  set tldr_path (string split ':' "$TLDR_PATH")
  switch (count $tldr_path)
    case  0
      tldr_die "TLDR_PATH unset?"
    case  1
      set plural ''
    case '*'
      set plural 's'
  end

  for path in $tldr_path
    set file "$path/$command.md"
    if test -f "$file"
      tldr_show "$file"
      tldr_die
    end
    set platform (string split '/' "$path")[-1]
    set -q searched
      and set searched $searched $platform
       or set searched           $platform
  end
  set platforms (string join ', ' $searched)
  tldr_die "No TLDR page for “$command” in section$plural $platforms"
end # tldr_find_and_show

function tldr_has_command --description 'Does CMD exist in $PATH?' --argument-names cmd
  test -n "$cmd"
    or tldr_die 'syntax: tldr_has_command CMD'
  command -v $cmd >/dev/null 2>&1
end

function tldr_update --description "Update TLDR pages from tlrd.sh"
  for cmd in mkdir touch stat
    tldr_has_command $cmd
      or tldr_die "tldr_update: no such command: $cmd"
  end

  mkdir --verbose --parents $TLDR_HOME/local $TLDR_HOME/pages/$TLDR_PLATFORMS
    or exit 1
  cd $TLDR_HOME
    or exit 1
  test -f $TLDR_ZIP
    or touch -t 197001010100.00 $TLDR_ZIP

  set -l oldage (stat --printf '%Y' $TLDR_ZIP)

  if tldr_has_command curl
    curl  $TLDR_CURL_OPTIONS $TLDR_ZIP $TLDR_ASSETS_URL
  else if tldr_has_command wget
    wget  $TLDR_WGET_OPTIONS $TLDR_ZIP $TLDR_ASSETS_URL
  else if tldr_has_command wget2
    wget2 $TLDR_WGET_OPTIONS $TLDR_ZIP $TLDR_ASSETS_URL
  else
    tldr_die 'cannot find curl, wget, or wget2 in $PATH'
  end

  set -l newage (stat --printf '%Y' $TLDR_ZIP)

  test "$newage" = "$oldage"
    and tldr_die

  if tldr_has_command bsdtar
    bsdtar $TLDR_BSDTAR_OPTIONS $TLDR_ZIP 'pages/'
      or exit 1
  else if tldr_has_command unzip
    unzip  $TLDR_UNZIP_OPTIONS  $TLDR_ZIP 'pages/*'
      or exit 1
  else
    tldr_die 'cannot find bsdtar or unzip in $PATH'
  end

end # tldr_update

# Main entry point
function tldr_main --description 'TLDR: Simplified and community-driven man pages'
  set arg_name "$TLDR_NAME"
  set arg_args $argv
  set arg_opts 'c/colour=' 'C-color=' 'D-debug' 'f/file=' 'h/help' \
               'p/platform=' 'u/update' 'V/version'

  # Lose the stacktrace in case of error
  argparse --name=$arg_name $arg_opts -- $arg_args 2>| read error
  test -n "$error"
    and tldr_die "$error" "$TLDR_TRY_HELP"

  set -q _flag_debug
    and set -g TLDR_DEBUG 'true'

  if set -q _flag_help
    tldr_help
    tldr_die
  else if set -q _flag_version
    tldr_version
    tldr_die
  end

  if set -q _flag_color
    set -q _flag_colour
      or set _flag_colour "$_flag_color"
  end

  if set -q _flag_colour
    contains "$_flag_colour" always auto never
      or tldr_die "Please choose colourising from always, auto or never"
    set -g TLDR_COLOURS "$_flag_colour"
  end

  # Set up environment and colours
  tldr_init

  if set -q _flag_update
    tldr_update
    tldr_die
  end

  # The gave us a full path to a .md file
  if set -q _flag_file
    tldr_show $_flag_file
    tldr_die
  end

  # If they gave us a platform …
  if set -q _flag_platform
    test -z "$_flag_platform"
      and tldr_die "Platform cannot be empty"
    test (string upper "$_flag_platform") = "PAGES"
      and tldr_die 'SRSLY?'
    if contains $_flag_platform $TLDR_PLATFORMS
      set -g TLDR_PATH "$TLDR_HOME/pages/$_flag_platform"
    else if test -d "$TLDR_HOME/$_flag_platform"
      set -g TLDR_PATH "$TLDR_HOME/$_flag_platform"
    else if test (string upper "$_flag_platform") = "ALL"
      set -g TLDR_PATH (string join ':' "$TLDR_HOME/local" $TLDR_HOME/pages/$TLDR_PLATFORMS)
    end
  end

  test (count $argv) -eq 0
    and tldr_die "Expected positional argument: COMMAND"

  if test (count $argv) -gt 1
    set unexpected $argv[2..-1]
    test (count $unexpected) -gt 1
      and set plural 's'
       or set plural ''
    set unexpected (string join ', ' $unexpected)
    tldr_die "Unexpected positional argument$plural: $unexpected"
  end

  tldr_find_and_show (string trim "$argv")
end # tldr_main

tldr_main $argv

# eof
